# ~/.dotfiles/home.nix

{ config, pkgs, ... }:

{
  # Home Manager needs information about you and the paths it should manage.
  home.username = "hans";
  home.homeDirectory = "/home/hans";

  home.stateVersion = "23.11"; 

  imports = [
    ./sh.nix      # shell imports
    ./lf.nix
    ./hvim.nix
#    ./firefox.nix
  ];

  # The home.packages option allows you to install Nix packages.
  home.packages = with pkgs; [
    eza lf bat zoxide starship fzf ripgrep tealdeer jq fd tree
    alacritty kitty meld rofi imagemagick tmux killall  
    unzip zip cron
    
  ];

  # Enable simple programs
  programs = {
    eza.enable      = true;
    bat.enable      = true;
    zoxide.enable   = true;
    starship.enable = true;
    fzf.enable      = true;
    ripgrep.enable  = true;
  };

  programs.kitty = {
    enable = true;
    font = {
      name = "JetBrainsMono Nerd Font";
      size = 9;
    };
    settings = {
      scrollback_lines = 2000;
      confirm_os_window_close = 0;
      background_opacity = "0.85";
    };
    extraConfig = ''
      foregound  #a9b1d6
      background #1a1b26
    '';
  };

  # Home Manager is pretty good at managing dotfiles.
  home.file = {
  };

  # Home Manager can also manage your environment variables.
  home.sessionVariables = {
    EDITOR   = "nvim";
    VISUAL   = "nvim";
    BROWSER  = "brave";
    TERMINAL = "kitty";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
