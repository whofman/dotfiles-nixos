# ~/.hvim - my own neovim setup

{ config, pkgs, ... }:

{
  programs.neovim = {
    enable = true;
    vimAlias = false;
    extraConfig = ''
      colorscheme gruvbox
      set mouse=a
      set number
      set relativenumber
      set termguicolors
      set splitright splitbelow
      set nowrap
      set clipboard=unnamedplus
      set expandtab
      set nobackup
      set undofile
      set undodir=~/.local/share/nvim/undodir
      set incsearch
      set nocompatible
      set noswapfile
      set hidden
    '';
    plugins = with pkgs.vimPlugins; [
      editorconfig-vim
      gruvbox-community
      vim-commentary
      vim-surround
    ];
  };
}

