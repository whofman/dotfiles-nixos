# NixOS hints and tips

- sudo nix-channel --update
- sudo nixos-rebuild switch

## Use flakes

- nix flake --update
- sudo nixos-rebuild switch --flake . [ #hostname ]

## Add Home Manager
- home-manager switch --flake . [ #username ]
